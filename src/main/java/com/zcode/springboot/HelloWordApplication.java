package com.zcode.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * SpringBoot应用标注
 */
@SpringBootApplication
public class HelloWordApplication {
  
  public static void main(String[] args) {
    SpringApplication.run(HelloWordApplication.class, args);
  }
}
